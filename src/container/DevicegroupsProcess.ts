
import { ProcessContainer } from 'pip-services3-container-node';

import { DevicegroupsServiceFactory } from '../build/DevicegroupsServiceFactory';
import { DefaultRpcFactory } from 'pip-services3-rpc-node';
import { DefaultGrpcFactory } from 'pip-services3-grpc-node';

export class DevicegroupsProcess extends ProcessContainer {

    public constructor() {
        super("ads-library", "Devicegroups microservice");
        this._factories.add(new DevicegroupsServiceFactory);
        this._factories.add(new DefaultRpcFactory);
        this._factories.add(new DefaultGrpcFactory);
    }

}
