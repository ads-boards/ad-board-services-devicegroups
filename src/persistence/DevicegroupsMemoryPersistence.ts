let _ = require('lodash');

import { FilterParams } from 'pip-services3-commons-node';
import { PagingParams } from 'pip-services3-commons-node';
import { DataPage } from 'pip-services3-commons-node';
import { IdentifiableMemoryPersistence } from 'pip-services3-data-node';
import { TagsProcessor } from 'pip-services3-commons-node';

import { DevicegroupV1 } from '../data/version1/DevicegroupV1';
import { IDevicegroupsPersistence } from './IDevicegroupsPersistence';

export class DevicegroupsMemoryPersistence
    extends IdentifiableMemoryPersistence<DevicegroupV1, string>
    implements IDevicegroupsPersistence {

    constructor() {
        super();
    }

    private matchString(value: string, search: string): boolean {
        if (value == null && search == null)
            return true;
        if (value == null || search == null)
            return false;
        return value.toLowerCase().indexOf(search) >= 0;
    }

    private matchSearch(item: DevicegroupV1, search: string): boolean {
        search = search.toLowerCase();
        if (this.matchString(item.id, search))
            return true;
        if (this.matchString(item.description, search))
            return true;
        return false;
    }

    private composeFilter(filter: FilterParams): any {
        filter = filter || new FilterParams();

        let search = filter.getAsNullableString('search');
        let id = filter.getAsNullableString('id');
        let device_ids = filter.getAsObject('device_ids');
        // Process device_ids filter
        if (_.isString(device_ids))
        device_ids = device_ids.split(',');
        if (!_.isArray(device_ids))
        device_ids = null;

        return (item) => {
            if (search && !this.matchSearch(item, search))
                return false;
            if (id && item.id != id)
                return false;
            if (device_ids != null && !(item.device_ids.some(ai => device_ids.includes(ai))))
                return false;

            return true;
        };
    }

    public getPageByFilter(correlationId: string, filter: FilterParams, paging: PagingParams,
        callback: (err: any, page: DataPage<DevicegroupV1>) => void): void {
        super.getPageByFilter(correlationId, this.composeFilter(filter), paging, null, null, callback);
    }

}
