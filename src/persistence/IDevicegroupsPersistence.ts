import { FilterParams } from 'pip-services3-commons-node';
import { PagingParams } from 'pip-services3-commons-node';
import { DataPage } from 'pip-services3-commons-node';
import { IGetter } from 'pip-services3-data-node';
import { IWriter } from 'pip-services3-data-node';

import { DevicegroupV1 } from '../data/version1/DevicegroupV1';

export interface IDevicegroupsPersistence extends IGetter<DevicegroupV1, string>, IWriter<DevicegroupV1, string> {
    getPageByFilter(correlationId: string, filter: FilterParams, paging: PagingParams, 
        callback: (err: any, page: DataPage<DevicegroupV1>) => void): void;

    getOneById(correlationId: string, id: string, 
        callback: (err: any, item: DevicegroupV1) => void): void;

    create(correlationId: string, item: DevicegroupV1, 
        callback: (err: any, item: DevicegroupV1) => void): void;

    update(correlationId: string, item: DevicegroupV1, 
        callback: (err: any, item: DevicegroupV1) => void): void;

    deleteById(correlationId: string, id: string,
        callback: (err: any, item: DevicegroupV1) => void): void;
}
