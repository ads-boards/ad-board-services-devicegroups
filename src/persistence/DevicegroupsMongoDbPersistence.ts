let _ = require('lodash');

import { FilterParams } from 'pip-services3-commons-node';
import { PagingParams } from 'pip-services3-commons-node';
import { DataPage } from 'pip-services3-commons-node';
import { IdentifiableMongoDbPersistence } from 'pip-services3-mongodb-node';

import { DevicegroupV1 } from '../data/version1/DevicegroupV1';
import { IDevicegroupsPersistence } from './IDevicegroupsPersistence';

export class DevicegroupsMongoDbPersistence
    extends IdentifiableMongoDbPersistence<DevicegroupV1, string>
    implements IDevicegroupsPersistence {

    constructor() {
        super('devicegroups');
    }

    private composeFilter(filter: any) {
        filter = filter || new FilterParams();

        let criteria = [];

        let search = filter.getAsNullableString('search');
        if (search != null) {
            let searchRegex = new RegExp(search, "i");
            let searchCriteria = [];
            searchCriteria.push({ id: { $regex: searchRegex } });
            searchCriteria.push({ description: { $regex: searchRegex } });
            criteria.push({ $or: searchCriteria });
        }

        let id = filter.getAsNullableString('id');
        if (id != null)
            criteria.push({ _id: id });

        let device_ids = filter.getAsObject('device_ids');
        if (_.isString(device_ids))
        device_ids = device_ids.split(',');
        if (_.isArray(device_ids))
            criteria.push({ device_ids: {$elemMatch: {$in: device_ids } }});

        return criteria.length > 0 ? { $and: criteria } : null;
    }

    public getPageByFilter(correlationId: string, filter: FilterParams, paging: PagingParams,
        callback: (err: any, page: DataPage<DevicegroupV1>) => void): void {
        super.getPageByFilter(correlationId, this.composeFilter(filter), paging, null, null, callback);
    }

}
