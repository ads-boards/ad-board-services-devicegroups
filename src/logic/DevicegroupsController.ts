import { ConfigParams } from 'pip-services3-commons-node';
import { IConfigurable } from 'pip-services3-commons-node';
import { IReferences } from 'pip-services3-commons-node';
import { Descriptor } from 'pip-services3-commons-node';
import { IReferenceable } from 'pip-services3-commons-node';
import { DependencyResolver } from 'pip-services3-commons-node';
import { FilterParams } from 'pip-services3-commons-node';
import { PagingParams } from 'pip-services3-commons-node';
import { DataPage } from 'pip-services3-commons-node';
import { ICommandable } from 'pip-services3-commons-node';
import { CommandSet } from 'pip-services3-commons-node';
import { TagsProcessor } from 'pip-services3-commons-node';

import { DevicegroupV1 } from '../data/version1/DevicegroupV1';
import { IDevicegroupsPersistence } from '../persistence/IDevicegroupsPersistence';
import { IDevicegroupsController } from './IDevicegroupsController';
import { DevicegroupsCommandSet } from './DevicegroupsCommandSet';

export class DevicegroupsController implements  IConfigurable, IReferenceable, ICommandable, IDevicegroupsController {
    private static _defaultConfig: ConfigParams = ConfigParams.fromTuples(
        'dependencies.persistence', 'ad-board-devicegroups:persistence:*:*:1.0'
    );

    private _dependencyResolver: DependencyResolver = new DependencyResolver(DevicegroupsController._defaultConfig);
    private _persistence: IDevicegroupsPersistence;
    private _commandSet: DevicegroupsCommandSet;

    public configure(config: ConfigParams): void {
        this._dependencyResolver.configure(config);
    }

    public setReferences(references: IReferences): void {
        this._dependencyResolver.setReferences(references);
        this._persistence = this._dependencyResolver.getOneRequired<IDevicegroupsPersistence>('persistence');
    }

    public getCommandSet(): CommandSet {
        if (this._commandSet == null)
            this._commandSet = new DevicegroupsCommandSet(this);
        return this._commandSet;
    }
    
    public getDevicegroups(correlationId: string, filter: FilterParams, paging: PagingParams, 
        callback: (err: any, page: DataPage<DevicegroupV1>) => void): void {
        this._persistence.getPageByFilter(correlationId, filter, paging, callback);
    }

    public getDevicegroupById(correlationId: string, id: string, 
        callback: (err: any, devicegroup: DevicegroupV1) => void): void {
        this._persistence.getOneById(correlationId, id, callback);        
    }

    public createDevicegroup(correlationId: string, devicegroup: DevicegroupV1, 
        callback: (err: any, devicegroup: DevicegroupV1) => void): void {
        this._persistence.create(correlationId, devicegroup, callback);
    }

    public updateDevicegroup(correlationId: string, devicegroup: DevicegroupV1, 
        callback: (err: any, devicegroup: DevicegroupV1) => void): void {
        this._persistence.update(correlationId, devicegroup, callback);
    }

    public deleteDevicegroupById(correlationId: string, id: string,
        callback: (err: any, devicegroup: DevicegroupV1) => void): void {  
        this._persistence.deleteById(correlationId, id, callback);
    }

}
