import { CommandSet } from 'pip-services3-commons-node';
import { ICommand } from 'pip-services3-commons-node';
import { Command } from 'pip-services3-commons-node';
import { Schema } from 'pip-services3-commons-node';
import { Parameters } from 'pip-services3-commons-node';
import { FilterParams } from 'pip-services3-commons-node';
import { PagingParams } from 'pip-services3-commons-node';
import { ObjectSchema } from 'pip-services3-commons-node';
import { TypeCode } from 'pip-services3-commons-node';
import { FilterParamsSchema } from 'pip-services3-commons-node';
import { PagingParamsSchema } from 'pip-services3-commons-node';

import { DevicegroupV1Schema } from '../data/version1/DevicegroupV1Schema';
import { IDevicegroupsController } from './IDevicegroupsController';

export class DevicegroupsCommandSet extends CommandSet {
    private _logic: IDevicegroupsController;

    constructor(logic: IDevicegroupsController) {
        super();

        this._logic = logic;

        // Register commands to the database
		this.addCommand(this.makeGetDevicegroupsCommand());
		this.addCommand(this.makeGetDevicegroupByIdCommand());
		this.addCommand(this.makeCreateDevicegroupCommand());
		this.addCommand(this.makeUpdateDevicegroupCommand());
		this.addCommand(this.makeDeleteDevicegroupByIdCommand());
    }

	private makeGetDevicegroupsCommand(): ICommand {
		return new Command(
			"get_devicegroups",
			new ObjectSchema(true)
				.withOptionalProperty('filter', new FilterParamsSchema())
				.withOptionalProperty('paging', new PagingParamsSchema()),
            (correlationId: string, args: Parameters, callback: (err: any, result: any) => void) => {
                let filter = FilterParams.fromValue(args.get("filter"));
                let paging = PagingParams.fromValue(args.get("paging"));
                this._logic.getDevicegroups(correlationId, filter, paging, callback);
            }
		);
	}

	private makeGetDevicegroupByIdCommand(): ICommand {
		return new Command(
			"get_devicegroup_by_id",
			new ObjectSchema(true)
				.withRequiredProperty('devicegroup_id', TypeCode.String),
            (correlationId: string, args: Parameters, callback: (err: any, result: any) => void) => {
                let devicegroup_id = args.getAsString("devicegroup_id");
                this._logic.getDevicegroupById(correlationId, devicegroup_id, callback);
            }
		);
	}

	private makeCreateDevicegroupCommand(): ICommand {
		return new Command(
			"create_devicegroup",
			new ObjectSchema(true)
				.withRequiredProperty('devicegroup', new DevicegroupV1Schema()),
            (correlationId: string, args: Parameters, callback: (err: any, result: any) => void) => {
                let devicegroup = args.get("devicegroup");
                this._logic.createDevicegroup(correlationId, devicegroup, callback);
            }
		);
	}

	private makeUpdateDevicegroupCommand(): ICommand {
		return new Command(
			"update_devicegroup",
			new ObjectSchema(true)
				.withRequiredProperty('devicegroup', new DevicegroupV1Schema()),
            (correlationId: string, args: Parameters, callback: (err: any, result: any) => void) => {
                let devicegroup = args.get("devicegroup");
                this._logic.updateDevicegroup(correlationId, devicegroup, callback);
            }
		);
	}
	
	private makeDeleteDevicegroupByIdCommand(): ICommand {
		return new Command(
			"delete_devicegroup_by_id",
			new ObjectSchema(true)
				.withRequiredProperty('devicegroup_id', TypeCode.String),
            (correlationId: string, args: Parameters, callback: (err: any, result: any) => void) => {
                let devicegroupId = args.getAsNullableString("devicegroup_id");
                this._logic.deleteDevicegroupById(correlationId, devicegroupId, callback);
			}
		);
	}

}