import { Factory } from 'pip-services3-components-node';
import { Descriptor } from 'pip-services3-commons-node';

import { DevicegroupsMongoDbPersistence } from '../persistence/DevicegroupsMongoDbPersistence';
import { DevicegroupsFilePersistence } from '../persistence/DevicegroupsFilePersistence';
import { DevicegroupsMemoryPersistence } from '../persistence/DevicegroupsMemoryPersistence';
import { DevicegroupsController } from '../logic/DevicegroupsController';
import { DevicegroupsHttpServiceV1 } from '../services/version1/DevicegroupsHttpServiceV1';


export class DevicegroupsServiceFactory extends Factory {
	public static Descriptor = new Descriptor("ad-board-devicegroups", "factory", "default", "default", "1.0");
	public static MemoryPersistenceDescriptor = new Descriptor("ad-board-devicegroups", "persistence", "memory", "*", "1.0");
	public static FilePersistenceDescriptor = new Descriptor("ad-board-devicegroups", "persistence", "file", "*", "1.0");
	public static MongoDbPersistenceDescriptor = new Descriptor("ad-board-devicegroups", "persistence", "mongodb", "*", "1.0");
	public static ControllerDescriptor = new Descriptor("ad-board-devicegroups", "controller", "default", "*", "1.0");
	public static HttpServiceDescriptor = new Descriptor("ad-board-devicegroups", "service", "http", "*", "1.0");
	
	constructor() {
		super();
		this.registerAsType(DevicegroupsServiceFactory.MemoryPersistenceDescriptor, DevicegroupsMemoryPersistence);
		this.registerAsType(DevicegroupsServiceFactory.FilePersistenceDescriptor, DevicegroupsFilePersistence);
		this.registerAsType(DevicegroupsServiceFactory.MongoDbPersistenceDescriptor, DevicegroupsMongoDbPersistence);
		this.registerAsType(DevicegroupsServiceFactory.ControllerDescriptor, DevicegroupsController);
		this.registerAsType(DevicegroupsServiceFactory.HttpServiceDescriptor, DevicegroupsHttpServiceV1);
		
	}
	
}
