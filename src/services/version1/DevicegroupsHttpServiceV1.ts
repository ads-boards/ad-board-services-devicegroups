import { Descriptor } from 'pip-services3-commons-node';
import { CommandableHttpService } from 'pip-services3-rpc-node';

export class DevicegroupsHttpServiceV1 extends CommandableHttpService {
    public constructor() {
        super('v1/devicegroups');
        this._dependencyResolver.put('controller', new Descriptor('ad-board-devicegroups', 'controller', 'default', '*', '1.0'));
    }
}