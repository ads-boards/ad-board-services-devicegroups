let _ = require('lodash');
let async = require('async');
let assert = require('chai').assert;

import { FilterParams, MultiString } from 'pip-services3-commons-node';
import { PagingParams } from 'pip-services3-commons-node';

import { DevicegroupV1 } from '../../src/data/version1/DevicegroupV1';

import { IDevicegroupsPersistence } from '../../src/persistence/IDevicegroupsPersistence';

let DEVICEGROUP1: DevicegroupV1 = {
    id: 'g1',
    device_ids: ['1', '2'],
    description: 'Title 1',

};
let DEVICEGROUP2: DevicegroupV1 = {
    id: 'g2',
    device_ids: ['1', '3'],
    description: 'Title 2',

};
let DEVICEGROUP3: DevicegroupV1 = {
    id: 'g3',
    device_ids: ['2', '3'],
    description: 'Title 3',
};

export class DevicegroupsPersistenceFixture {
    private _persistence: IDevicegroupsPersistence;

    constructor(persistence) {
        assert.isNotNull(persistence);
        this._persistence = persistence;
    }

    private testCreateDevicegroups(done) {
        async.series([
            // Create one devicegroup
            (callback) => {
                this._persistence.create(
                    null,
                    DEVICEGROUP1,
                    (err, devicegroup) => {
                        assert.isNull(err);

                        assert.isObject(devicegroup);
                        assert.equal(devicegroup.id, DEVICEGROUP1.id);
                        assert.equal(devicegroup.description, DEVICEGROUP1.description);

                        callback();
                    }
                );
            },
            // Create another devicegroup
            (callback) => {
                this._persistence.create(
                    null,
                    DEVICEGROUP2,
                    (err, devicegroup) => {
                        assert.isNull(err);

                        assert.isObject(devicegroup);
                        assert.equal(devicegroup.id, DEVICEGROUP2.id);
                        assert.equal(devicegroup.description, DEVICEGROUP2.description);

                        callback();
                    }
                );
            },
            // Create yet another devicegroup
            (callback) => {
                this._persistence.create(
                    null,
                    DEVICEGROUP3,
                    (err, devicegroup) => {
                        assert.isNull(err);

                        assert.isObject(devicegroup);
                        assert.equal(devicegroup.id, DEVICEGROUP3.id);
                        assert.equal(devicegroup.description, DEVICEGROUP3.description);
        
                        callback();
                    }
                );
            }
        ], done);
    }

    testCrudOperations(done) {
        let devicegroup1: DevicegroupV1;

        async.series([
            // Create items
            (callback) => {
                this.testCreateDevicegroups(callback);
            },
            // Get all devicegroups
            (callback) => {
                this._persistence.getPageByFilter(
                    null,
                    new FilterParams(),
                    new PagingParams(),
                    (err, page) => {
                        assert.isNull(err);

                        assert.isObject(page);
                        assert.lengthOf(page.data, 3);

                        devicegroup1 = page.data[0];

                        callback();
                    }
                );
            },
            // Update the devicegroup
            (callback) => {
                devicegroup1.description = 'Updated Text 1';

                this._persistence.update(
                    null,
                    devicegroup1,
                    (err, devicegroup) => {
                        assert.isNull(err);

                        assert.isObject(devicegroup);
                        assert.equal(devicegroup.description, 'Updated Text 1');
                        assert.equal(devicegroup.id, devicegroup1.id);

                        callback();
                    }
                );
            },
            // Delete devicegroup
            (callback) => {
                this._persistence.deleteById(
                    null,
                    devicegroup1.id,
                    (err) => {
                        assert.isNull(err);

                        callback();
                    }
                );
            },
            // Try to get delete devicegroup
            (callback) => {
                this._persistence.getOneById(
                    null,
                    devicegroup1.id,
                    (err, devicegroup) => {
                        assert.isNull(err);

                        assert.isNull(devicegroup || null);

                        callback();
                    }
                );
            }
        ], done);
    }

    testGetWithFilter(done) {
        async.series([
            // Create devicegroups
            (callback) => {
                this.testCreateDevicegroups(callback);
            },
            // Get devicegroups filtered by id
            (callback) => {
                this._persistence.getPageByFilter(
                    null,
                    FilterParams.fromValue({
                        id: 'g1'
                    }),
                    new PagingParams(),
                    (err, devicegroups) => {
                        assert.isNull(err);

                        assert.isObject(devicegroups);
                        assert.lengthOf(devicegroups.data, 1);

                        callback();
                    }
                );
            },
            // Get devicegroups filtered by publish_group_ids
            (callback) => {
                this._persistence.getPageByFilter(
                    null,
                    FilterParams.fromValue({
                        device_ids: ['1']
                    }),
                    new PagingParams(),
                    (err, devicegroups) => {
                        assert.isNull(err);

                        assert.isObject(devicegroups);
                        assert.lengthOf(devicegroups.data, 2);

                        callback();
                    }
                );
            },
            // Get devicegroups filtered by publish_group_ids
            (callback) => {
                this._persistence.getPageByFilter(
                    null,
                    FilterParams.fromValue({
                        device_ids: ['2', '3']
                    }),
                    new PagingParams(),
                    (err, devicegroups) => {
                        assert.isNull(err);

                        assert.isObject(devicegroups);
                        assert.lengthOf(devicegroups.data, 3);

                        callback();
                    }
                );
            },
            // Get devicegroups filtered by search
            (callback) => {
                this._persistence.getPageByFilter(
                    null,
                    FilterParams.fromValue({
                        search: 'Title'
                    }),
                    new PagingParams(),
                    (err, devicegroups) => {
                        assert.isNull(err);

                        assert.isObject(devicegroups);
                        assert.lengthOf(devicegroups.data, 3);

                        callback();
                    }
                );
            }
        ], done);
    }

}
