import { ConfigParams } from 'pip-services3-commons-node';

import { DevicegroupsMemoryPersistence } from '../../src/persistence/DevicegroupsMemoryPersistence';
import { DevicegroupsPersistenceFixture } from './DevicegroupsPersistenceFixture';

suite('DevicegroupsMemoryPersistence', ()=> {
    let persistence: DevicegroupsMemoryPersistence;
    let fixture: DevicegroupsPersistenceFixture;
    
    setup((done) => {
        persistence = new DevicegroupsMemoryPersistence();
        persistence.configure(new ConfigParams());
        
        fixture = new DevicegroupsPersistenceFixture(persistence);
        
        persistence.open(null, done);
    });
    
    teardown((done) => {
        persistence.close(null, done);
    });
        
    test('CRUD Operations', (done) => {
        fixture.testCrudOperations(done);
    });

    test('Get with Filters', (done) => {
        fixture.testGetWithFilter(done);
    });

});