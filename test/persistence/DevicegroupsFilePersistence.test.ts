import { ConfigParams } from 'pip-services3-commons-node';

import { DevicegroupsFilePersistence } from '../../src/persistence/DevicegroupsFilePersistence';
import { DevicegroupsPersistenceFixture } from './DevicegroupsPersistenceFixture';

suite('DevicegroupsFilePersistence', ()=> {
    let persistence: DevicegroupsFilePersistence;
    let fixture: DevicegroupsPersistenceFixture;
    
    setup((done) => {
        persistence = new DevicegroupsFilePersistence('./data/ads.test.json');

        fixture = new DevicegroupsPersistenceFixture(persistence);

        persistence.open(null, (err) => {
            persistence.clear(null, done);
        });
    });
    
    teardown((done) => {
        persistence.close(null, done);
    });
        
    test('CRUD Operations', (done) => {
        fixture.testCrudOperations(done);
    });

    test('Get with Filters', (done) => {
        fixture.testGetWithFilter(done);
    });

});