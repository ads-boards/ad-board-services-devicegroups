"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DevicegroupsServiceFactory = void 0;
const pip_services3_components_node_1 = require("pip-services3-components-node");
const pip_services3_commons_node_1 = require("pip-services3-commons-node");
const DevicegroupsMongoDbPersistence_1 = require("../persistence/DevicegroupsMongoDbPersistence");
const DevicegroupsFilePersistence_1 = require("../persistence/DevicegroupsFilePersistence");
const DevicegroupsMemoryPersistence_1 = require("../persistence/DevicegroupsMemoryPersistence");
const DevicegroupsController_1 = require("../logic/DevicegroupsController");
const DevicegroupsHttpServiceV1_1 = require("../services/version1/DevicegroupsHttpServiceV1");
class DevicegroupsServiceFactory extends pip_services3_components_node_1.Factory {
    constructor() {
        super();
        this.registerAsType(DevicegroupsServiceFactory.MemoryPersistenceDescriptor, DevicegroupsMemoryPersistence_1.DevicegroupsMemoryPersistence);
        this.registerAsType(DevicegroupsServiceFactory.FilePersistenceDescriptor, DevicegroupsFilePersistence_1.DevicegroupsFilePersistence);
        this.registerAsType(DevicegroupsServiceFactory.MongoDbPersistenceDescriptor, DevicegroupsMongoDbPersistence_1.DevicegroupsMongoDbPersistence);
        this.registerAsType(DevicegroupsServiceFactory.ControllerDescriptor, DevicegroupsController_1.DevicegroupsController);
        this.registerAsType(DevicegroupsServiceFactory.HttpServiceDescriptor, DevicegroupsHttpServiceV1_1.DevicegroupsHttpServiceV1);
    }
}
exports.DevicegroupsServiceFactory = DevicegroupsServiceFactory;
DevicegroupsServiceFactory.Descriptor = new pip_services3_commons_node_1.Descriptor("ad-board-devicegroups", "factory", "default", "default", "1.0");
DevicegroupsServiceFactory.MemoryPersistenceDescriptor = new pip_services3_commons_node_1.Descriptor("ad-board-devicegroups", "persistence", "memory", "*", "1.0");
DevicegroupsServiceFactory.FilePersistenceDescriptor = new pip_services3_commons_node_1.Descriptor("ad-board-devicegroups", "persistence", "file", "*", "1.0");
DevicegroupsServiceFactory.MongoDbPersistenceDescriptor = new pip_services3_commons_node_1.Descriptor("ad-board-devicegroups", "persistence", "mongodb", "*", "1.0");
DevicegroupsServiceFactory.ControllerDescriptor = new pip_services3_commons_node_1.Descriptor("ad-board-devicegroups", "controller", "default", "*", "1.0");
DevicegroupsServiceFactory.HttpServiceDescriptor = new pip_services3_commons_node_1.Descriptor("ad-board-devicegroups", "service", "http", "*", "1.0");
//# sourceMappingURL=DevicegroupsServiceFactory.js.map