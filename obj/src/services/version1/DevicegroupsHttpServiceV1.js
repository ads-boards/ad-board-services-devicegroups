"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DevicegroupsHttpServiceV1 = void 0;
const pip_services3_commons_node_1 = require("pip-services3-commons-node");
const pip_services3_rpc_node_1 = require("pip-services3-rpc-node");
class DevicegroupsHttpServiceV1 extends pip_services3_rpc_node_1.CommandableHttpService {
    constructor() {
        super('v1/devicegroups');
        this._dependencyResolver.put('controller', new pip_services3_commons_node_1.Descriptor('ad-board-devicegroups', 'controller', 'default', '*', '1.0'));
    }
}
exports.DevicegroupsHttpServiceV1 = DevicegroupsHttpServiceV1;
//# sourceMappingURL=DevicegroupsHttpServiceV1.js.map