import { CommandSet } from 'pip-services3-commons-node';
import { IDevicegroupsController } from './IDevicegroupsController';
export declare class DevicegroupsCommandSet extends CommandSet {
    private _logic;
    constructor(logic: IDevicegroupsController);
    private makeGetDevicegroupsCommand;
    private makeGetDevicegroupByIdCommand;
    private makeCreateDevicegroupCommand;
    private makeUpdateDevicegroupCommand;
    private makeDeleteDevicegroupByIdCommand;
}
