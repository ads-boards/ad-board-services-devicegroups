import { DataPage } from 'pip-services3-commons-node';
import { FilterParams } from 'pip-services3-commons-node';
import { PagingParams } from 'pip-services3-commons-node';
import { DevicegroupV1 } from '../data/version1/DevicegroupV1';
export interface IDevicegroupsController {
    getDevicegroups(correlationId: string, filter: FilterParams, paging: PagingParams, callback: (err: any, page: DataPage<DevicegroupV1>) => void): void;
    getDevicegroupById(correlationId: string, devicegroup_id: string, callback: (err: any, devicegroup: DevicegroupV1) => void): void;
    createDevicegroup(correlationId: string, devicegroup: DevicegroupV1, callback: (err: any, devicegroup: DevicegroupV1) => void): void;
    updateDevicegroup(correlationId: string, devicegroup: DevicegroupV1, callback: (err: any, devicegroup: DevicegroupV1) => void): void;
    deleteDevicegroupById(correlationId: string, devicegroup_id: string, callback: (err: any, devicegroup: DevicegroupV1) => void): void;
}
