import { ConfigParams } from 'pip-services3-commons-node';
import { IConfigurable } from 'pip-services3-commons-node';
import { IReferences } from 'pip-services3-commons-node';
import { IReferenceable } from 'pip-services3-commons-node';
import { FilterParams } from 'pip-services3-commons-node';
import { PagingParams } from 'pip-services3-commons-node';
import { DataPage } from 'pip-services3-commons-node';
import { ICommandable } from 'pip-services3-commons-node';
import { CommandSet } from 'pip-services3-commons-node';
import { DevicegroupV1 } from '../data/version1/DevicegroupV1';
import { IDevicegroupsController } from './IDevicegroupsController';
export declare class DevicegroupsController implements IConfigurable, IReferenceable, ICommandable, IDevicegroupsController {
    private static _defaultConfig;
    private _dependencyResolver;
    private _persistence;
    private _commandSet;
    configure(config: ConfigParams): void;
    setReferences(references: IReferences): void;
    getCommandSet(): CommandSet;
    getDevicegroups(correlationId: string, filter: FilterParams, paging: PagingParams, callback: (err: any, page: DataPage<DevicegroupV1>) => void): void;
    getDevicegroupById(correlationId: string, id: string, callback: (err: any, devicegroup: DevicegroupV1) => void): void;
    createDevicegroup(correlationId: string, devicegroup: DevicegroupV1, callback: (err: any, devicegroup: DevicegroupV1) => void): void;
    updateDevicegroup(correlationId: string, devicegroup: DevicegroupV1, callback: (err: any, devicegroup: DevicegroupV1) => void): void;
    deleteDevicegroupById(correlationId: string, id: string, callback: (err: any, devicegroup: DevicegroupV1) => void): void;
}
