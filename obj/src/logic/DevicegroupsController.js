"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DevicegroupsController = void 0;
const pip_services3_commons_node_1 = require("pip-services3-commons-node");
const pip_services3_commons_node_2 = require("pip-services3-commons-node");
const DevicegroupsCommandSet_1 = require("./DevicegroupsCommandSet");
class DevicegroupsController {
    constructor() {
        this._dependencyResolver = new pip_services3_commons_node_2.DependencyResolver(DevicegroupsController._defaultConfig);
    }
    configure(config) {
        this._dependencyResolver.configure(config);
    }
    setReferences(references) {
        this._dependencyResolver.setReferences(references);
        this._persistence = this._dependencyResolver.getOneRequired('persistence');
    }
    getCommandSet() {
        if (this._commandSet == null)
            this._commandSet = new DevicegroupsCommandSet_1.DevicegroupsCommandSet(this);
        return this._commandSet;
    }
    getDevicegroups(correlationId, filter, paging, callback) {
        this._persistence.getPageByFilter(correlationId, filter, paging, callback);
    }
    getDevicegroupById(correlationId, id, callback) {
        this._persistence.getOneById(correlationId, id, callback);
    }
    createDevicegroup(correlationId, devicegroup, callback) {
        this._persistence.create(correlationId, devicegroup, callback);
    }
    updateDevicegroup(correlationId, devicegroup, callback) {
        this._persistence.update(correlationId, devicegroup, callback);
    }
    deleteDevicegroupById(correlationId, id, callback) {
        this._persistence.deleteById(correlationId, id, callback);
    }
}
exports.DevicegroupsController = DevicegroupsController;
DevicegroupsController._defaultConfig = pip_services3_commons_node_1.ConfigParams.fromTuples('dependencies.persistence', 'ad-board-devicegroups:persistence:*:*:1.0');
//# sourceMappingURL=DevicegroupsController.js.map