"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DevicegroupsCommandSet = void 0;
const pip_services3_commons_node_1 = require("pip-services3-commons-node");
const pip_services3_commons_node_2 = require("pip-services3-commons-node");
const pip_services3_commons_node_3 = require("pip-services3-commons-node");
const pip_services3_commons_node_4 = require("pip-services3-commons-node");
const pip_services3_commons_node_5 = require("pip-services3-commons-node");
const pip_services3_commons_node_6 = require("pip-services3-commons-node");
const pip_services3_commons_node_7 = require("pip-services3-commons-node");
const pip_services3_commons_node_8 = require("pip-services3-commons-node");
const DevicegroupV1Schema_1 = require("../data/version1/DevicegroupV1Schema");
class DevicegroupsCommandSet extends pip_services3_commons_node_1.CommandSet {
    constructor(logic) {
        super();
        this._logic = logic;
        // Register commands to the database
        this.addCommand(this.makeGetDevicegroupsCommand());
        this.addCommand(this.makeGetDevicegroupByIdCommand());
        this.addCommand(this.makeCreateDevicegroupCommand());
        this.addCommand(this.makeUpdateDevicegroupCommand());
        this.addCommand(this.makeDeleteDevicegroupByIdCommand());
    }
    makeGetDevicegroupsCommand() {
        return new pip_services3_commons_node_2.Command("get_devicegroups", new pip_services3_commons_node_5.ObjectSchema(true)
            .withOptionalProperty('filter', new pip_services3_commons_node_7.FilterParamsSchema())
            .withOptionalProperty('paging', new pip_services3_commons_node_8.PagingParamsSchema()), (correlationId, args, callback) => {
            let filter = pip_services3_commons_node_3.FilterParams.fromValue(args.get("filter"));
            let paging = pip_services3_commons_node_4.PagingParams.fromValue(args.get("paging"));
            this._logic.getDevicegroups(correlationId, filter, paging, callback);
        });
    }
    makeGetDevicegroupByIdCommand() {
        return new pip_services3_commons_node_2.Command("get_devicegroup_by_id", new pip_services3_commons_node_5.ObjectSchema(true)
            .withRequiredProperty('devicegroup_id', pip_services3_commons_node_6.TypeCode.String), (correlationId, args, callback) => {
            let devicegroup_id = args.getAsString("devicegroup_id");
            this._logic.getDevicegroupById(correlationId, devicegroup_id, callback);
        });
    }
    makeCreateDevicegroupCommand() {
        return new pip_services3_commons_node_2.Command("create_devicegroup", new pip_services3_commons_node_5.ObjectSchema(true)
            .withRequiredProperty('devicegroup', new DevicegroupV1Schema_1.DevicegroupV1Schema()), (correlationId, args, callback) => {
            let devicegroup = args.get("devicegroup");
            this._logic.createDevicegroup(correlationId, devicegroup, callback);
        });
    }
    makeUpdateDevicegroupCommand() {
        return new pip_services3_commons_node_2.Command("update_devicegroup", new pip_services3_commons_node_5.ObjectSchema(true)
            .withRequiredProperty('devicegroup', new DevicegroupV1Schema_1.DevicegroupV1Schema()), (correlationId, args, callback) => {
            let devicegroup = args.get("devicegroup");
            this._logic.updateDevicegroup(correlationId, devicegroup, callback);
        });
    }
    makeDeleteDevicegroupByIdCommand() {
        return new pip_services3_commons_node_2.Command("delete_devicegroup_by_id", new pip_services3_commons_node_5.ObjectSchema(true)
            .withRequiredProperty('devicegroup_id', pip_services3_commons_node_6.TypeCode.String), (correlationId, args, callback) => {
            let devicegroupId = args.getAsNullableString("devicegroup_id");
            this._logic.deleteDevicegroupById(correlationId, devicegroupId, callback);
        });
    }
}
exports.DevicegroupsCommandSet = DevicegroupsCommandSet;
//# sourceMappingURL=DevicegroupsCommandSet.js.map