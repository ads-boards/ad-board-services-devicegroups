import { FilterParams } from 'pip-services3-commons-node';
import { PagingParams } from 'pip-services3-commons-node';
import { DataPage } from 'pip-services3-commons-node';
import { IdentifiableMemoryPersistence } from 'pip-services3-data-node';
import { DevicegroupV1 } from '../data/version1/DevicegroupV1';
import { IDevicegroupsPersistence } from './IDevicegroupsPersistence';
export declare class DevicegroupsMemoryPersistence extends IdentifiableMemoryPersistence<DevicegroupV1, string> implements IDevicegroupsPersistence {
    constructor();
    private matchString;
    private matchSearch;
    private composeFilter;
    getPageByFilter(correlationId: string, filter: FilterParams, paging: PagingParams, callback: (err: any, page: DataPage<DevicegroupV1>) => void): void;
}
