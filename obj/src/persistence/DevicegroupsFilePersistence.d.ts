import { ConfigParams } from 'pip-services3-commons-node';
import { JsonFilePersister } from 'pip-services3-data-node';
import { DevicegroupsMemoryPersistence } from './DevicegroupsMemoryPersistence';
import { DevicegroupV1 } from '../data/version1/DevicegroupV1';
export declare class DevicegroupsFilePersistence extends DevicegroupsMemoryPersistence {
    protected _persister: JsonFilePersister<DevicegroupV1>;
    constructor(path?: string);
    configure(config: ConfigParams): void;
}
