"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DevicegroupsFilePersistence = void 0;
const pip_services3_data_node_1 = require("pip-services3-data-node");
const DevicegroupsMemoryPersistence_1 = require("./DevicegroupsMemoryPersistence");
class DevicegroupsFilePersistence extends DevicegroupsMemoryPersistence_1.DevicegroupsMemoryPersistence {
    constructor(path) {
        super();
        this._persister = new pip_services3_data_node_1.JsonFilePersister(path);
        this._loader = this._persister;
        this._saver = this._persister;
    }
    configure(config) {
        super.configure(config);
        this._persister.configure(config);
    }
}
exports.DevicegroupsFilePersistence = DevicegroupsFilePersistence;
//# sourceMappingURL=DevicegroupsFilePersistence.js.map