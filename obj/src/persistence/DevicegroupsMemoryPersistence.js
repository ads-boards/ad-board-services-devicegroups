"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DevicegroupsMemoryPersistence = void 0;
let _ = require('lodash');
const pip_services3_commons_node_1 = require("pip-services3-commons-node");
const pip_services3_data_node_1 = require("pip-services3-data-node");
class DevicegroupsMemoryPersistence extends pip_services3_data_node_1.IdentifiableMemoryPersistence {
    constructor() {
        super();
    }
    matchString(value, search) {
        if (value == null && search == null)
            return true;
        if (value == null || search == null)
            return false;
        return value.toLowerCase().indexOf(search) >= 0;
    }
    matchSearch(item, search) {
        search = search.toLowerCase();
        if (this.matchString(item.id, search))
            return true;
        if (this.matchString(item.description, search))
            return true;
        return false;
    }
    composeFilter(filter) {
        filter = filter || new pip_services3_commons_node_1.FilterParams();
        let search = filter.getAsNullableString('search');
        let id = filter.getAsNullableString('id');
        let device_ids = filter.getAsObject('device_ids');
        // Process device_ids filter
        if (_.isString(device_ids))
            device_ids = device_ids.split(',');
        if (!_.isArray(device_ids))
            device_ids = null;
        return (item) => {
            if (search && !this.matchSearch(item, search))
                return false;
            if (id && item.id != id)
                return false;
            if (device_ids != null && !(item.device_ids.some(ai => device_ids.includes(ai))))
                return false;
            return true;
        };
    }
    getPageByFilter(correlationId, filter, paging, callback) {
        super.getPageByFilter(correlationId, this.composeFilter(filter), paging, null, null, callback);
    }
}
exports.DevicegroupsMemoryPersistence = DevicegroupsMemoryPersistence;
//# sourceMappingURL=DevicegroupsMemoryPersistence.js.map