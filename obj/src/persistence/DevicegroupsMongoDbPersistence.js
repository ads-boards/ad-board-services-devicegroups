"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DevicegroupsMongoDbPersistence = void 0;
let _ = require('lodash');
const pip_services3_commons_node_1 = require("pip-services3-commons-node");
const pip_services3_mongodb_node_1 = require("pip-services3-mongodb-node");
class DevicegroupsMongoDbPersistence extends pip_services3_mongodb_node_1.IdentifiableMongoDbPersistence {
    constructor() {
        super('devicegroups');
    }
    composeFilter(filter) {
        filter = filter || new pip_services3_commons_node_1.FilterParams();
        let criteria = [];
        let search = filter.getAsNullableString('search');
        if (search != null) {
            let searchRegex = new RegExp(search, "i");
            let searchCriteria = [];
            searchCriteria.push({ id: { $regex: searchRegex } });
            searchCriteria.push({ description: { $regex: searchRegex } });
            criteria.push({ $or: searchCriteria });
        }
        let id = filter.getAsNullableString('id');
        if (id != null)
            criteria.push({ _id: id });
        let device_ids = filter.getAsObject('device_ids');
        if (_.isString(device_ids))
            device_ids = device_ids.split(',');
        if (_.isArray(device_ids))
            criteria.push({ device_ids: { $elemMatch: { $in: device_ids } } });
        return criteria.length > 0 ? { $and: criteria } : null;
    }
    getPageByFilter(correlationId, filter, paging, callback) {
        super.getPageByFilter(correlationId, this.composeFilter(filter), paging, null, null, callback);
    }
}
exports.DevicegroupsMongoDbPersistence = DevicegroupsMongoDbPersistence;
//# sourceMappingURL=DevicegroupsMongoDbPersistence.js.map