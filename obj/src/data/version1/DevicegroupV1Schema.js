"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DevicegroupV1Schema = void 0;
const pip_services3_commons_node_1 = require("pip-services3-commons-node");
const pip_services3_commons_node_2 = require("pip-services3-commons-node");
const pip_services3_commons_node_3 = require("pip-services3-commons-node");
class DevicegroupV1Schema extends pip_services3_commons_node_1.ObjectSchema {
    constructor() {
        super();
        this.withOptionalProperty('id', pip_services3_commons_node_3.TypeCode.String);
        this.withRequiredProperty('device_ids', new pip_services3_commons_node_2.ArraySchema(pip_services3_commons_node_3.TypeCode.String));
        this.withRequiredProperty('description', pip_services3_commons_node_3.TypeCode.String);
    }
}
exports.DevicegroupV1Schema = DevicegroupV1Schema;
//# sourceMappingURL=DevicegroupV1Schema.js.map