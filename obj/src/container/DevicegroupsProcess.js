"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DevicegroupsProcess = void 0;
const pip_services3_container_node_1 = require("pip-services3-container-node");
const DevicegroupsServiceFactory_1 = require("../build/DevicegroupsServiceFactory");
const pip_services3_rpc_node_1 = require("pip-services3-rpc-node");
const pip_services3_grpc_node_1 = require("pip-services3-grpc-node");
class DevicegroupsProcess extends pip_services3_container_node_1.ProcessContainer {
    constructor() {
        super("ads-library", "Devicegroups microservice");
        this._factories.add(new DevicegroupsServiceFactory_1.DevicegroupsServiceFactory);
        this._factories.add(new pip_services3_rpc_node_1.DefaultRpcFactory);
        this._factories.add(new pip_services3_grpc_node_1.DefaultGrpcFactory);
    }
}
exports.DevicegroupsProcess = DevicegroupsProcess;
//# sourceMappingURL=DevicegroupsProcess.js.map