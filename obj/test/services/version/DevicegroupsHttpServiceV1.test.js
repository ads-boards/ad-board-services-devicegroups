"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
let _ = require('lodash');
let async = require('async');
let restify = require('restify');
let assert = require('chai').assert;
const pip_services3_commons_node_1 = require("pip-services3-commons-node");
const pip_services3_commons_node_2 = require("pip-services3-commons-node");
const pip_services3_commons_node_3 = require("pip-services3-commons-node");
const DevicegroupsMemoryPersistence_1 = require("../../../src/persistence/DevicegroupsMemoryPersistence");
const DevicegroupsController_1 = require("../../../src/logic/DevicegroupsController");
const DevicegroupsHttpServiceV1_1 = require("../../../src/services/version1/DevicegroupsHttpServiceV1");
let httpConfig = pip_services3_commons_node_1.ConfigParams.fromTuples("connection.protocol", "http", "connection.host", "localhost", "connection.port", 3000);
let DEVICEGROUP1 = {
    id: '1',
    device_ids: ['1', '2'],
    description: 'Title 1',
};
let DEVICEGROUP2 = {
    id: '2',
    device_ids: ['1', '3'],
    description: 'Title 2',
};
suite('DevicegroupsHttpServiceV1', () => {
    let service;
    let rest;
    suiteSetup((done) => {
        let persistence = new DevicegroupsMemoryPersistence_1.DevicegroupsMemoryPersistence();
        let controller = new DevicegroupsController_1.DevicegroupsController();
        service = new DevicegroupsHttpServiceV1_1.DevicegroupsHttpServiceV1();
        service.configure(httpConfig);
        let references = pip_services3_commons_node_3.References.fromTuples(new pip_services3_commons_node_2.Descriptor('ad-board-devicegroups', 'persistence', 'memory', 'default', '1.0'), persistence, new pip_services3_commons_node_2.Descriptor('ad-board-devicegroups', 'controller', 'default', 'default', '1.0'), controller, new pip_services3_commons_node_2.Descriptor('ad-board-devicegroups', 'service', 'http', 'default', '1.0'), service);
        controller.setReferences(references);
        service.setReferences(references);
        service.open(null, done);
    });
    suiteTeardown((done) => {
        service.close(null, done);
    });
    setup(() => {
        let url = 'http://localhost:3000';
        rest = restify.createJsonClient({ url: url, version: '*' });
    });
    test('CRUD Operations', (done) => {
        let devicegroup1, devicegroup2;
        async.series([
            // Create one devicegroup
            (callback) => {
                rest.post('/v1/devicegroups/create_devicegroup', {
                    devicegroup: DEVICEGROUP1
                }, (err, req, res, devicegroup) => {
                    assert.isNull(err);
                    assert.isObject(devicegroup);
                    assert.equal(devicegroup.id, DEVICEGROUP1.id);
                    assert.equal(devicegroup.description, DEVICEGROUP1.description);
                    devicegroup1 = devicegroup;
                    callback();
                });
            },
            // Create another devicegroup
            (callback) => {
                rest.post('/v1/devicegroups/create_devicegroup', {
                    devicegroup: DEVICEGROUP2
                }, (err, req, res, devicegroup) => {
                    assert.isNull(err);
                    assert.isObject(devicegroup);
                    assert.equal(devicegroup.id, DEVICEGROUP2.id);
                    assert.equal(devicegroup.description, DEVICEGROUP2.description);
                    devicegroup2 = devicegroup;
                    callback();
                });
            },
            // Get all devicegroups
            (callback) => {
                rest.post('/v1/devicegroups/get_devicegroups', {}, (err, req, res, page) => {
                    assert.isNull(err);
                    assert.isObject(page);
                    assert.lengthOf(page.data, 2);
                    callback();
                });
            },
            // Update the devicegroup
            (callback) => {
                devicegroup1.text = 'Updated Name 1';
                rest.post('/v1/devicegroups/update_devicegroup', {
                    devicegroup: devicegroup1
                }, (err, req, res, devicegroup) => {
                    assert.isNull(err);
                    assert.isObject(devicegroup);
                    assert.equal(devicegroup.text, 'Updated Name 1');
                    assert.equal(devicegroup.id, DEVICEGROUP1.id);
                    devicegroup1 = devicegroup;
                    callback();
                });
            },
            // Delete devicegroup
            (callback) => {
                rest.post('/v1/devicegroups/delete_devicegroup_by_id', {
                    devicegroup_id: devicegroup1.id
                }, (err, req, res, result) => {
                    assert.isNull(err);
                    //assert.isNull(result);
                    callback();
                });
            },
            // Try to get delete devicegroup
            (callback) => {
                rest.post('/v1/devicegroups/get_devicegroup_by_id', {
                    devicegroup_id: devicegroup1.id
                }, (err, req, res, result) => {
                    assert.isNull(err);
                    //assert.isNull(result);
                    callback();
                });
            }
        ], done);
    });
});
//# sourceMappingURL=DevicegroupsHttpServiceV1.test.js.map