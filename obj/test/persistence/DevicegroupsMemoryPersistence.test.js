"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const pip_services3_commons_node_1 = require("pip-services3-commons-node");
const DevicegroupsMemoryPersistence_1 = require("../../src/persistence/DevicegroupsMemoryPersistence");
const DevicegroupsPersistenceFixture_1 = require("./DevicegroupsPersistenceFixture");
suite('DevicegroupsMemoryPersistence', () => {
    let persistence;
    let fixture;
    setup((done) => {
        persistence = new DevicegroupsMemoryPersistence_1.DevicegroupsMemoryPersistence();
        persistence.configure(new pip_services3_commons_node_1.ConfigParams());
        fixture = new DevicegroupsPersistenceFixture_1.DevicegroupsPersistenceFixture(persistence);
        persistence.open(null, done);
    });
    teardown((done) => {
        persistence.close(null, done);
    });
    test('CRUD Operations', (done) => {
        fixture.testCrudOperations(done);
    });
    test('Get with Filters', (done) => {
        fixture.testGetWithFilter(done);
    });
});
//# sourceMappingURL=DevicegroupsMemoryPersistence.test.js.map