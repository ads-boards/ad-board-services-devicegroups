"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const DevicegroupsFilePersistence_1 = require("../../src/persistence/DevicegroupsFilePersistence");
const DevicegroupsPersistenceFixture_1 = require("./DevicegroupsPersistenceFixture");
suite('DevicegroupsFilePersistence', () => {
    let persistence;
    let fixture;
    setup((done) => {
        persistence = new DevicegroupsFilePersistence_1.DevicegroupsFilePersistence('./data/ads.test.json');
        fixture = new DevicegroupsPersistenceFixture_1.DevicegroupsPersistenceFixture(persistence);
        persistence.open(null, (err) => {
            persistence.clear(null, done);
        });
    });
    teardown((done) => {
        persistence.close(null, done);
    });
    test('CRUD Operations', (done) => {
        fixture.testCrudOperations(done);
    });
    test('Get with Filters', (done) => {
        fixture.testGetWithFilter(done);
    });
});
//# sourceMappingURL=DevicegroupsFilePersistence.test.js.map