"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DevicegroupsPersistenceFixture = void 0;
let _ = require('lodash');
let async = require('async');
let assert = require('chai').assert;
const pip_services3_commons_node_1 = require("pip-services3-commons-node");
const pip_services3_commons_node_2 = require("pip-services3-commons-node");
let DEVICEGROUP1 = {
    id: 'g1',
    device_ids: ['1', '2'],
    description: 'Title 1',
};
let DEVICEGROUP2 = {
    id: 'g2',
    device_ids: ['1', '3'],
    description: 'Title 2',
};
let DEVICEGROUP3 = {
    id: 'g3',
    device_ids: ['2', '3'],
    description: 'Title 3',
};
class DevicegroupsPersistenceFixture {
    constructor(persistence) {
        assert.isNotNull(persistence);
        this._persistence = persistence;
    }
    testCreateDevicegroups(done) {
        async.series([
            // Create one devicegroup
            (callback) => {
                this._persistence.create(null, DEVICEGROUP1, (err, devicegroup) => {
                    assert.isNull(err);
                    assert.isObject(devicegroup);
                    assert.equal(devicegroup.id, DEVICEGROUP1.id);
                    assert.equal(devicegroup.description, DEVICEGROUP1.description);
                    callback();
                });
            },
            // Create another devicegroup
            (callback) => {
                this._persistence.create(null, DEVICEGROUP2, (err, devicegroup) => {
                    assert.isNull(err);
                    assert.isObject(devicegroup);
                    assert.equal(devicegroup.id, DEVICEGROUP2.id);
                    assert.equal(devicegroup.description, DEVICEGROUP2.description);
                    callback();
                });
            },
            // Create yet another devicegroup
            (callback) => {
                this._persistence.create(null, DEVICEGROUP3, (err, devicegroup) => {
                    assert.isNull(err);
                    assert.isObject(devicegroup);
                    assert.equal(devicegroup.id, DEVICEGROUP3.id);
                    assert.equal(devicegroup.description, DEVICEGROUP3.description);
                    callback();
                });
            }
        ], done);
    }
    testCrudOperations(done) {
        let devicegroup1;
        async.series([
            // Create items
            (callback) => {
                this.testCreateDevicegroups(callback);
            },
            // Get all devicegroups
            (callback) => {
                this._persistence.getPageByFilter(null, new pip_services3_commons_node_1.FilterParams(), new pip_services3_commons_node_2.PagingParams(), (err, page) => {
                    assert.isNull(err);
                    assert.isObject(page);
                    assert.lengthOf(page.data, 3);
                    devicegroup1 = page.data[0];
                    callback();
                });
            },
            // Update the devicegroup
            (callback) => {
                devicegroup1.description = 'Updated Text 1';
                this._persistence.update(null, devicegroup1, (err, devicegroup) => {
                    assert.isNull(err);
                    assert.isObject(devicegroup);
                    assert.equal(devicegroup.description, 'Updated Text 1');
                    assert.equal(devicegroup.id, devicegroup1.id);
                    callback();
                });
            },
            // Delete devicegroup
            (callback) => {
                this._persistence.deleteById(null, devicegroup1.id, (err) => {
                    assert.isNull(err);
                    callback();
                });
            },
            // Try to get delete devicegroup
            (callback) => {
                this._persistence.getOneById(null, devicegroup1.id, (err, devicegroup) => {
                    assert.isNull(err);
                    assert.isNull(devicegroup || null);
                    callback();
                });
            }
        ], done);
    }
    testGetWithFilter(done) {
        async.series([
            // Create devicegroups
            (callback) => {
                this.testCreateDevicegroups(callback);
            },
            // Get devicegroups filtered by id
            (callback) => {
                this._persistence.getPageByFilter(null, pip_services3_commons_node_1.FilterParams.fromValue({
                    id: 'g1'
                }), new pip_services3_commons_node_2.PagingParams(), (err, devicegroups) => {
                    assert.isNull(err);
                    assert.isObject(devicegroups);
                    assert.lengthOf(devicegroups.data, 1);
                    callback();
                });
            },
            // Get devicegroups filtered by publish_group_ids
            (callback) => {
                this._persistence.getPageByFilter(null, pip_services3_commons_node_1.FilterParams.fromValue({
                    device_ids: ['1']
                }), new pip_services3_commons_node_2.PagingParams(), (err, devicegroups) => {
                    assert.isNull(err);
                    assert.isObject(devicegroups);
                    assert.lengthOf(devicegroups.data, 2);
                    callback();
                });
            },
            // Get devicegroups filtered by publish_group_ids
            (callback) => {
                this._persistence.getPageByFilter(null, pip_services3_commons_node_1.FilterParams.fromValue({
                    device_ids: ['2', '3']
                }), new pip_services3_commons_node_2.PagingParams(), (err, devicegroups) => {
                    assert.isNull(err);
                    assert.isObject(devicegroups);
                    assert.lengthOf(devicegroups.data, 3);
                    callback();
                });
            },
            // Get devicegroups filtered by search
            (callback) => {
                this._persistence.getPageByFilter(null, pip_services3_commons_node_1.FilterParams.fromValue({
                    search: 'Title'
                }), new pip_services3_commons_node_2.PagingParams(), (err, devicegroups) => {
                    assert.isNull(err);
                    assert.isObject(devicegroups);
                    assert.lengthOf(devicegroups.data, 3);
                    callback();
                });
            }
        ], done);
    }
}
exports.DevicegroupsPersistenceFixture = DevicegroupsPersistenceFixture;
//# sourceMappingURL=DevicegroupsPersistenceFixture.js.map